#include <iostream>
#include "LinkedList.h"
#include "Node.h" //for tests

int main()
{
    std::cout << "Let's make lists!" << std::endl;
    LinkedList<std::string> DioQuotes;



    DioQuotes.addNode("WRRRY");
    DioQuotes.addNode("MUDA MUDA MUDA");
    DioQuotes.addNode("ZA WARUDO");
    DioQuotes.addNode("ROADA ROLLA DA");

    DioQuotes.print();
    std::cout << std::endl;
    std::cout << "SORTING!!!" << std::endl;
    DioQuotes.quickSort(0, DioQuotes.getSize()-1);
    DioQuotes.print();
    std::cout << std::endl;

    std::cout << "REVERSE!" << std::endl;
    DioQuotes.reverse();
    DioQuotes.print();
    std::cout << std::endl;

    std::cout << "ADDING!" << std::endl;
    DioQuotes.insert(3, "KONO DIO DA");
    DioQuotes.print();

    std::cout << std::endl;
    std::cout << "ENDING" << std::endl;
    return 0;
}

int additional()
{

    LinkedList<int> numberList;
    numberList.addNode(4);
    numberList.addNode(6);
    numberList.addNode(1);
    numberList.addNode(5);
    numberList.addNode(2);
    numberList.addNode(3);
    numberList.print();
    std::cout << "SORTING!!!" << std::endl;
    numberList.quickSort(0, numberList.getSize()-1);
    numberList.print();

    //std::cout << DioQuotes[1]  << std::endl;
    std::cout << numberList[2] << std::endl;

    //Nodes
    Node<int> n1(6);
    Node<int> n2(5);

    //Pointers To Nodes
    Node<std::string> *n3 = new Node<std::string>("Tortoise");
    Node<std::string> *n4 = new Node<std::string>("Aardvark");

    // Testing < operator
    if (n1 < n2)
    {
        std::cout << "n1 is less than n2." << std::endl;
    }

    //pointers have to be dereferenced for the comparison to be correct
    if (*n3 < *n4)
    {
        std::cout << "n3 is less than n4." << std::endl;
    }

    if (*n3 != *n4)
    {
        std::cout << "n3 is not equal to n4." << std::endl;
    }

    std::cout << "Testing modifyData()" << std::endl;
    std::cout << "numberList1 is: " << numberList.getNodeData(1) << std::endl;
    numberList.swap(1, 2);
    std::cout << "numberList1 is: " << numberList.getNodeData(1) << std::endl;

    // delete nodes made with the pointers
    delete n3;
    delete n4;
    return 0;
}
