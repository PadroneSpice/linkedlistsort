#include "LinkedList.h"

/** Create and add node to the list */
template<typename Data>
void LinkedList<Data>::addNode(Data d)
{
    // If the list is empty, then made this node the head and tail.
    if (!head)
    {
      head         = new NODE(d);
      tail         = head;
    }
    else
    {
        tail->next = new NODE(d);
        tail->next->prev = tail;
        tail       = tail->next;
    }

    // Update the size of the list.
    size++;
}

/** Legacy Function to calculate the size of the list */
template<typename Data>
void LinkedList<Data>::calculateSize()
{
    if (!head)         { size = 0; }

    NODE *ptr                = head;
    u_int sizeCounter        =    0;
    while (ptr != nullptr)
    {
        sizeCounter++;
        ptr = ptr->next;
    }
    size = sizeCounter;
}

/** Read-only access to size of the list */
template<typename Data>
u_int const LinkedList<Data>::getSize()
{ return size; }


/** Read-only access to the data inside the node at the given index */
template<typename Data>
Data const LinkedList<Data>::getNodeData(u_int index)
{
    NODE *ptr = head;

    if (index > size-1)
    {
        std::cout << "getNodeData(): Requested index " << index << " out of bounds! Using index 0!" << std::endl;
        return ptr->data;
    }

    // Travel to the specified index and return the data.
    for (u_int i=0; i<index; i++) { ptr = ptr->next; }

    return ptr->data;
}

/** Insert node with Data d at the given index. */
template<typename Data>
void LinkedList<Data>::insert(u_int index, Data d)
{
    NODE *N = new NODE(d);
    if (index == 0)
    {
        N->next = head;
        head->prev = N;
        head = N;
    }
    else if (index == size)
    {
        N->prev = tail;
        tail->next = N;
        tail = N;
    }
    else
    {
        NODE *curr = head;
        for (u_int i=0; i<index-1; i++) // move curr directly before insertion site
            curr = curr->next;
        N->next = curr->next;
        N->prev = curr;
        curr->next = N;
        N->next->prev = N;
    }
    size++;
}

/** Write the given data to node at the given index */
template<typename Data>
void LinkedList<Data>::modifyData(u_int index, Data d)
{
    NODE *ptr = head;

    if (index > size-1)
    {
        std::cout << "modifyData(): Requested index " << index << " out of bounds! Using index 0!" << std::endl;
        ptr->data = d;
        return;
    }

    // Travel to the specified index and modify the data.
    for (u_int i=0; i<index; i++) { ptr = ptr->next; }

    ptr->data = d;
}

/** Print the contents of the list */
template<typename Data>
void LinkedList<Data>::print()
{
    for (u_int index=0; index<size; index++)
    {
        std::cout << getNodeData(index) << std::endl;
    }
}

/** Quick Sort with leftmost index and rightmost index */
template<typename Data>
void LinkedList<Data>::quickSort(u_int left, u_int right)
{
    if (left >= right) { return; } // if left index is bigger,
                                   // the subarray size would be <= 1.

    // choose the last item of the subarray as the pivot
    Data pivot = getNodeData(right);

    // sort
    u_int cnt = left;
    for (u_int i=left; i<=right; i++)
    {
        if (getNodeData(i) <= pivot)
        {
            swap(i, cnt);
            cnt++;
        }
    }

    // call on left side of pivot
    quickSort(left, cnt-2);
    // call on right side of pivot
    quickSort(cnt,  right);
}

/** Swap the data in nodes of the two given indices. */
template<typename Data>
void LinkedList<Data>::swap(u_int a, u_int b)
{
    Data temp =    getNodeData(a);
    modifyData(a,  getNodeData(b));
    modifyData(b,           temp);
}

/** Reverse the linked list by its pointers. */
template<typename Data>
void LinkedList<Data>::reverse()
{
    // basic doubly-linked list reverse algorithm
    // from geeksforgeeks.org
    NODE *curr     = head;
    NODE *headCopy = head;
    NODE *temp     = nullptr;

    while (curr != nullptr)
    {
       temp = curr->prev;
       curr->prev = curr->next;
       curr->next = temp;
       curr = curr->prev;
    }

    if (temp != nullptr)
        head = temp->prev;
    tail = headCopy;
}

/** Destructor */
template<typename Data>
LinkedList<Data>::~LinkedList()
{
    NODE *ptr1, *ptr2;
    ptr1 = head;
    while (ptr1 != nullptr)
    {
        ptr2 = ptr1->next;
        delete ptr1;
        ptr1 = ptr2;
    }
}

//required to compile correctly; find better way to do this in the header file
template class LinkedList<std::string>;
template class LinkedList<int>;
template class LinkedList<char>;  //untested
template class LinkedList<float>; //untested
