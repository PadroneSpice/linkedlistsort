#use g++ for everything
CC= g++  	

# include debugging symbols in object files,
# and enable all warnings
CXXFLAGS= -g -Wall -std=c++11

#include debugging symbols in executable
LDFLAGS= -g

default:
	g++ Node.cpp LinkedList.cpp main.cpp -o main
clean:
	$(RM) main *.o
