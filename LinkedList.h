#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED
#include "Node.h"
#include <iostream>

typedef unsigned int u_int;

template<typename Data> class LinkedList
{
    typedef       Node<Data> NODE;

    private:
        NODE *head, *tail;
        u_int        size;
    public:
        /* constructor */
        LinkedList<Data>()
        {
            size = 0;
            head = nullptr;
            tail = nullptr;
        };

        void        addNode(Data d);
        void        calculateSize(); // legacy; obsolete now that 'size' is a member variable
                                     // and addNode() increments the count but keeping just in case

        u_int const getSize();
        Data  const getNodeData(u_int index);
        void        insert(u_int index, Data d);
        void        modifyData(u_int index, Data d);
        void        print();
        void        quickSort(u_int left, u_int right);
        void        swap(u_int a, u_int b);
        void        reverse();
        ~LinkedList();

        /* Operator Overloads */

        //Note: This just returns the stored data, not the node itself.
        //It's just an extension of getNodeData().
        Data operator[](u_int index) { return this->getNodeData(index);}
};


#endif // LINKEDLIST_H_INCLUDED
