#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED
#include <ostream>
#include <iostream>


template<typename Data> class Node
{
    typedef          Node<Data> NODE;
    typedef const    Node<Data> CONST_NODE;

    private:

    public:
        // Constructor
        Node(const Data & d) : data(d)
        {
            next = nullptr;
            prev = nullptr;
            data = d;
        }
        Data data;          // data the node contains
        NODE *next;         // pointer to the next node
        NODE *prev;         // pointer to the previous node


        // Operator overloads use & to use reference instead of make a copy.
        // The node is const so that it can't be modified.
        bool operator==(CONST_NODE &RHS) { return this->data == RHS.data; }

        bool operator!=(CONST_NODE &RHS) { return this->data != RHS.data; }

        bool operator< (CONST_NODE &RHS) { return this->data <  RHS.data; }

        bool operator<=(CONST_NODE &RHS)
                     { return this->data < RHS || this->data == RHS.data; }

        ~Node() {} // destructor's default behavior is sufficient
};

// Overload to allow node data printing in cout
template<typename Data>
std::ostream & operator<<(std::ostream & os, const Node<Data> & n)
{
    os << n.data;
    return os;
}


#endif // NODE_H_INCLUDED
